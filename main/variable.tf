variable "var_project" {
  default = "mrjim-101"
}
variable "var_env" {
  default = "dev"
}
variable "var_company" {
  default = "mrjim"
}
variable "asia-northeast1_private_subnet" {
  default = "10.26.1.0/24"
}
variable "asia-northeast1_public_subnet" {
  default = "10.26.2.0/24"
}
variable "asia-southeast1_private_subnet" {
  default = "10.26.3.0/24"
}
variable "asia-southeast1_public_subnet" {
  default = "10.26.4.0/24"
}
