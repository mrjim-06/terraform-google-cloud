provider "google" {
  project     = "${var.var_project}"
}
module "vpc" {
  source = "../modules/global" 
  env                   = "${var.var_env}"
  company               = "${var.var_company}"
  var_asia-northeast1_public_subnet = "${var.asia-northeast1_public_subnet}"
  var_asia-northeast1_private_subnet= "${var.asia-northeast1_private_subnet}"
  var_asia-southeast1_public_subnet = "${var.asia-southeast1_public_subnet}"
  var_asia-southeast1_private_subnet= "${var.asia-southeast1_private_subnet}"
}
module "asia-northeast1" {
  source                = "../modules/asia-northeast1"
  network_self_link     = "${module.vpc.out_vpc_self_link}"
  subnetwork1           = "${module.asia-northeast1.asia-northeast1_out_public_subnet_name}"
  env                   = "${var.var_env}"
  company               = "${var.var_company}"
  var_asia-northeast1_public_subnet = "${var.asia-northeast1_public_subnet}"
  var_asia-northeast1_private_subnet= "${var.asia-northeast1_private_subnet}"
}
module "asia-southeast1" {
  source                = "../modules/asia-southeast1"
  network_self_link     = "${module.vpc.out_vpc_self_link}"
  subnetwork1           = "${module.asia-southeast1.asia-southeast1_out_public_subnet_name}"
  env                   = "${var.var_env}"
  company               = "${var.var_company}"
  var_asia-southeast1_public_subnet = "${var.asia-southeast1_public_subnet}"
  var_asia-southeast1_private_subnet= "${var.asia-southeast1_private_subnet}"
}
######################################################################
# Display Output Public Instance                                     #
######################################################################
output "asia-northeast1_public_address"  { value = "${module.asia-northeast1.asia-northeast1_pub_address}"}
output "asia-northeast1_private_address" { value = "${module.asia-northeast1.asia-northeast1_pri_address}"}
output "asia-southeast1_public_address"  { value = "${module.asia-southeast1.asia-southeast1_pub_address}"}
output "asia-southeast1_private_address" { value = "${module.asia-southeast1.asia-southeast1_pri_address}"}
output "vpc_self_link" { value = "${module.vpc.out_vpc_self_link}"}